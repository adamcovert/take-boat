$(document).ready(function () {
  svg4everybody();

  // Open main navigation menu
  $('.hamburger').on('click', function () {
    if ( !$('.main-nav').hasClass('main-nav--is-active') ) {
      $('.main-nav').addClass('main-nav--is-active');
    } else {
      $('.main-nav').removeClass('main-nav--is-active');
    }
  });

  // CLose nain navigation menu
  $('.main-nav__close-btn').on('click', function () {
    if ( $('.main-nav').hasClass('main-nav--is-active') ) {
      $('.main-nav').removeClass('main-nav--is-active');
    }
  });

  // Open / CLose search form in main promo page
  $('.search-form__input').on('input', function () {
    if ($(this).val().trim().length > 0) {
      $(this).parent().addClass('search-form--is-active');
    } else {
      $(this).parent().removeClass('search-form--is-active');
    }
  });

  // Open Start form
  $('.search-form__submit').on('click', function () {
    $.fancybox.open($('#select-form'), {
      smallBtn: false,
      infobar : false,
      buttons : false
    });
  });

   // CLose Start form
   $('.select-type-of-service .close-up-btn').on('click', function () {
    $.fancybox.close($('#select-form'));
  });

  // Open / Close selected block
  $('.selected__close-btn').on('click', function () {
    if ( !$('.selected').hasClass('selected--is-active') ) {
      $('.selected').addClass('selected--is-active');
    } else {
      $('.selected').removeClass('selected--is-active');
    }
  });

  // Open / Close need to select block
  $('.need-to-select__close-btn').on('click', function () {
    if ( !$('.need-to-select').hasClass('need-to-select--is-active') ) {
      $('.need-to-select').addClass('need-to-select--is-active');
    } else {
      $('.need-to-select').removeClass('need-to-select--is-active');
    }
  });



  // Open About boat full info
  $('.boat-card__additional-info-btn').on('click', function () {
    $('.boat-full-info').addClass('boat-full-info--is-active');
  });

  $('.boat-full-info__close-btn').on('click', function () {
    $('.boat-full-info').removeClass('boat-full-info--is-active');
  });



  $("[data-fancybox]").fancybox();



  // Gallery in Boat full info
  $('.boat-full-info__photo-gallery-slider').owlCarousel({
    items: 1,
    responsive: {
      0: {
        nav: false
      },
      992: {
        nav: true,
        navText: ['<svg viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#FFFFFF" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>', '<svg viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#FFFFFF" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>'],
      }
    }
  });



  // Star rating in ordering complete section
  $('.star.rating').click(function () {
    $(this).parent().attr('data-stars', $(this).data('rating'));
  });



  // Date picker
  $(".calendar").datepicker({
    dateFormat: 'dd.mm.y',
    firstDay: 1,
    showOtherMonths: true,
    selectOtherMonths: true
  });

  $(document).on('click', '.date-picker .input', function(e){
    $parent = $(this).parents('.date-picker');
    $parent.toggleClass('open');
  });

  $(".calendar").on("change",function(){
    var $me = $(this),
    $selected = $me.val(),
    $parent = $me.parents('.date-picker');
    $parent.find('.result').children('span').html($selected);
  });



  // Time picker
  $('.timepicker').wickedpicker({
    twentyFour: true,
    upArrow: 'wickedpicker__controls__control-up',
    downArrow: 'wickedpicker__controls__control-down',
    close: 'wickedpicker__close',
    hoverState: 'hover-state',
    showSeconds: false,
    timeSeparator: ' : ',
    secondsInterval: 1,
    minutesInterval: 1,
    beforeShow: null,
    afterShow: null,
    show: null,
    clearable: false,
  });



  if (window.matchMedia("(max-width: 767px)").matches) {

    $(document).on('click', '[data-object]', function () {

      if ($(this).hasClass('route-select') ||
          $(this).hasClass('boat-select') ||
          $(this).hasClass('pier-select')) {
        if ($(this).hasClass('is-active')) {
          $(this).siblings().each(function () {
            $(this).removeClass('is-active');
          });
        } else {
          $(this).addClass('is-active');
          $(this).siblings().each(function () {
            $(this).removeClass('is-active');
          });
        }
      }

      if ($(this).hasClass('route-select') ||
          $(this).hasClass('boat-select') ||
          $(this).hasClass('pier-select')) {
        $('.need-to-select').addClass('need-to-select--is-active');
      }

      if ($(this).hasClass('route-card') ||
          $(this).hasClass('boat-card') ||
          $(this).hasClass('pier-card')) {
        $('.need-to-select').removeClass('need-to-select--is-active');
      }

      if ($(this).hasClass('route-select') && $(this).hasClass('is-active')) {
        $('.need-to-select__title').text('Выберите маршрут');
        $('.need-to-select__list').children().each(function () {
          if ($(this).hasClass('route-card')) {
            $(this).css('display', 'block');
          } else {
            $(this).css('display', 'none');
          };

          $(this).on('click', function () {
            if ($(this).attr('data-route')) {
              $('.route-select').attr('data-select', 'need-to-select');
              var htmlInline = $(this).html();
              $('.route-select').html(htmlInline);
              $('.route-select').attr('data-select', 'selected');
            }
          })
        });
      } else if ($(this).hasClass('boat-select') && $(this).hasClass('is-active')) {
        $('.need-to-select__title').text('Выберите судно');
        $('.need-to-select__list').children().each(function () {
          if ($(this).hasClass('boat-card')) {
            $(this).css('display', 'block');
          } else {
            $(this).css('display', 'none');
          };

          $(this).on('click', function () {
            if ($(this).attr('data-boat')) {
              $('.boat-select').attr('data-select', 'need-to-select');
              var htmlInline = $(this).html();
              $('.boat-select').html(htmlInline);
              $('.boat-select').attr('data-select', 'selected');
            }
          })
        });
      } else if ($(this).hasClass('pier-select') && $(this).hasClass('is-active')) {
        $('.need-to-select__title').text('Выберите причал');
        $('.need-to-select__list').children().each(function () {
          if ($(this).hasClass('pier-card')) {
            $(this).css('display', 'block');
          } else {
            $(this).css('display', 'none');
          };

          $(this).on('click', function () {
            if ($(this).attr('data-pier')) {
              $('.pier-select').attr('data-select', 'need-to-select');
              var htmlInline = $(this).html();
              $('.pier-select').html(htmlInline);
              $('.pier-select').attr('data-select', 'selected');
            }
          })
        });
      };
    });
  }

  // Card Add Template
  if (window.matchMedia("(min-width: 768px)").matches) {

    if ($('.route-select').hasClass('is-active')) {
      $('.need-to-select__title').text('Выберите маршрут');
      $('.need-to-select__list').children().each(function () {
        $(this).on('click', function () {
          if ($(this).attr('data-route')) {
            $('.route-select').attr('data-select', 'need-to-select');
            var htmlInline = $(this).html();
            $('.route-select').html(htmlInline);
            $('.route-select').attr('data-select', 'selected');
          }
        });
      });
    }

    $(document).on('click', '[data-object]', function () {

      if ($(this).hasClass('route-select') || $(this).hasClass('boat-select') || $(this).hasClass('pier-select')) {
        if ($(this).hasClass('is-active')) {
          $(this).siblings().each(function () {
            $(this).removeClass('is-active');
          });
        } else {
          $(this).addClass('is-active');
          $(this).siblings().each(function () {
            $(this).removeClass('is-active');
          });
        }
      }

      if ($(this).hasClass('route-select') && $(this).hasClass('is-active')) {
        $('.need-to-select__title').text('Выберите маршрут');
        $('.need-to-select__list').children().each(function () {
          if ($(this).hasClass('route-card')) {
            $(this).css('display', 'block');
          } else {
            $(this).css('display', 'none');
          };

          $(this).on('click', function () {
            if ($(this).attr('data-route')) {
              $('.route-select').attr('data-select', 'need-to-select');
              var htmlInline = $(this).html();
              $('.route-select').html(htmlInline);
              $('.route-select').attr('data-select', 'selected');
            }
          })
        });
      } else if ($(this).hasClass('boat-select') && $(this).hasClass('is-active')) {
        $('.need-to-select__title').text('Выберите судно');
        $('.need-to-select__list').children().each(function () {
          if ($(this).hasClass('boat-card')) {
            $(this).css('display', 'block');
          } else {
            $(this).css('display', 'none');
          };

          $(this).on('click', function () {
            if ($(this).attr('data-boat')) {
              $('.boat-select').attr('data-select', 'need-to-select');
              var htmlInline = $(this).html();
              $('.boat-select').html(htmlInline);
              $('.boat-select').attr('data-select', 'selected');
            }
          })
        });
      } else if ($(this).hasClass('pier-select') && $(this).hasClass('is-active')) {
        $('.need-to-select__title').text('Выберите причал');
        $('.need-to-select__list').children().each(function () {
          if ($(this).hasClass('pier-card')) {
            $(this).css('display', 'block');
          } else {
            $(this).css('display', 'none');
          };

          $(this).on('click', function () {
            if ($(this).attr('data-pier')) {
              $('.pier-select').attr('data-select', 'need-to-select');
              var htmlInline = $(this).html();
              $('.pier-select').html(htmlInline);
              $('.pier-select').attr('data-select', 'selected');
            }
          })
        });
      };
    });
  }



  // Sign in / Sign up / Ordering
  $('#go-to-registration').on('click', function () {
    $('.sign-in').css('display', 'none');
    $('.sign-up').css('display', 'block');
  });

  $('#go-to-ordering').on('click', function () {
    $('.sign-up').css('display', 'none');
    $('.ordering').css('display', 'block');
  });

  $('#go-to-complete').on('click', function () {
    $('.ordering').css('display', 'none');
    $('.order-is-completed').css('display', 'block');
  });



  // People count
  $('#people-count').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;

    $this.addClass('select-hidden');
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });

    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });
  });
});