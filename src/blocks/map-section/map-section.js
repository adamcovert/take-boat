var firstRoute = [
  {lat: 59.95058049, lng: 30.34755355},
  {lat: 59.95307321, lng: 30.34763938},
  {lat: 59.95527711, lng: 30.33905632},
  {lat: 59.9728915, lng: 30.33054008},
  {lat: 59.97963404, lng: 30.31766547},
  {lat: 59.98152343, lng: 30.27887},
  {lat: 59.97933345, lng: 30.2290505},
  {lat: 59.97594085, lng: 30.2061337},
  {lat: 59.96816661, lng: 30.21102605},
  {lat: 59.9464664, lng: 30.29552455},
  {lat: 59.94436008, lng: 30.31157489},
  {lat: 59.93616834, lng: 30.29022611},
  {lat: 59.93449137, lng: 30.29211438},
  {lat: 59.95058049, lng: 30.34755355}
];

var secondRoute = [
  {lat: 58.95058049, lng: 30.34755355},
  {lat: 58.95307321, lng: 30.34763938},
  {lat: 58.95527711, lng: 30.33905632},
  {lat: 58.9728915, lng: 30.33054008},
  {lat: 58.97963404, lng: 30.31766547},
  {lat: 58.98152343, lng: 30.27887},
  {lat: 58.97933345, lng: 30.2290505},
  {lat: 58.97594085, lng: 30.2061337},
  {lat: 58.96816661, lng: 30.21102605},
  {lat: 59.9464664, lng: 30.29552455},
  {lat: 59.94436008, lng: 30.31157489},
  {lat: 59.93616834, lng: 30.29022611},
  {lat: 59.93449137, lng: 30.29211438},
  {lat: 58.95058049, lng: 30.34755355}
];

var map;
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 59.950160947, lng: 30.31612718},
    zoom: 12
  });

  var routePath = new google.maps.Polyline({
    path: [],
    geodesic: true,
    strokeColor: '#0070bf',
    strokeOpacity: 1.0,
    strokeWeight: 3
  });

  routePath.setMap(map);

  $('[data-route]').each( function () {
    $(this).on('click', function () {
      if ( $(this).attr('data-route') === 'first' ) {
        routePath.setPath(firstRoute);
      } else if ( $(this).attr('data-route') === 'second' ) {
        routePath.setPath(secondRoute);
      }
    })
  })
}

